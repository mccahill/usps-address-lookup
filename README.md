# USPS Address Lookup

A minimal Kulai Build integration microservice to validate USPS addresses using the 
US Postal Service API. The USPS API matches poorly-formed user entered addresses with 
deliverable addresses and can in many cases fill in missing fields.

![](images/example.png)

Build likes JSON, the USPS likes XML, so we need a bit of data reformatting to let 
Build forms validate user-entered postal addresses.

###  Build and run

To run this, you need a valid USPS API account - get one here:
	 https://www.usps.com/business/web-tools-apis/welcome.htm

and take a look at the API docs here:
     https://www.usps.com/business/web-tools-apis/address-information-api.pdf
     

This microservice application is packaged as a Docker container. Build the 
container using the build shell script, then run it using the run shell script 
(or Docker Compose  if that is how you roll).

At runtime, you need to pass the container an environment variable (USPS_SECRET)
with your USPS API user ID. Note that the USPS API accounts have both an
user ID and a password, but *only* the user ID is used when calling the API. So
you will need to edit the run shell script to include your USPS API user ID.

### Integrating with Kulai Build

Define a new integration in the Kuali admin web page with these parameters:

- type of integration: Get Single Data Item
- HTTP Method: POST
- Integration URL: http://your.server.name.here.edu:8080/address/
- Authentication Type: No Authentication
- ID Key: usps_address1
- Label Key: usps_address1 
- URL inputs: here you need to map user-entered fields to inputs that will be sent as a JSON object to the microservice. The address lookup service will expect to see these items, but they are not required to be present:
   - address1
   - address2
   - city
   - state
   - zipcode
   - name
   - company
- Outputs: the address lookup will return output fields which you can then map to form fields. Some output fields are not always populated (for example: the usps_error field). Protip: use the conditioonal visibility feature in the form design to only display usps_error and usps_return_text fields when they have content.
   - usps_return_text
   - usps_error
   - usps_address1
   - usps_address2
   - usps_city
   - usps_state
   - usps_zip5
   - usps_zip4
   - usps_zip_all
 
### What is next?

This microservice was a quick and dirty experiment to see how hard it would be to integrate an arbitrary API into Kuali. Note that this leans heavily on a nice python package 
     https://github.com/brobin/usps-api
that supports features like trasking and creating shipments. Perhaps someone will add those features so that an approved form could crewate a shipment and then track the shipment progress.

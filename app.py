# Load tornado module
from tornado.web import Application, RequestHandler, HTTPError
from tornado.ioloop import IOLoop
import os
from usps import USPSApi, Address
import json



class AddressHandler(RequestHandler):

    def post(self):
      data = json.loads(self.request.body)
      usps = USPSApi(usps_secret, test=True)
      address = Address(
             name = str(data["name"]),
             company = str(data["company"]),
             address_1 = str(data["address1"]),
             address_2 = str(data["address2"]),
             city = str(data["city"]),
             state = str(data["state"]),
             zipcode = str(data["zipcode"] )) 

      validation = usps.validate_address(address)
      clean_result = { 'usps_error': '',
                       'usps_return_text': '',
                       'usps_address1': '',
                       'usps_address2': '',
                       'usps_city': '',
                       'usps_state': '',
                       'usps_zip5': '',
                       'usps_zip_ext': '',
                       'usps_zip_all': ''}
      address_result = validation.result['AddressValidateResponse']['Address']
      result_keys = address_result.keys()
      if 'Error' in result_keys:
        clean_result['usps_error'] = address_result['Error']['Description']
      if 'ReturnText' in result_keys:
        clean_result['usps_return_text'] = address_result['ReturnText']
      if 'Name' in result_keys:
        clean_result['usps_name'] = address_result['Name']
      if 'Company' in result_keys:
        clean_result['usps_company'] = address_result['Company']
      if 'Address1' in result_keys:
        clean_result['usps_address1'] = address_result['Address1']
      if 'Address2' in result_keys:
        clean_result['usps_address2'] = address_result['Address2']
      if 'City' in result_keys:
        clean_result['usps_city'] = address_result['City']
      if 'State' in result_keys:
        clean_result['usps_state'] = address_result['State']
      if 'Zip5' in result_keys:
        clean_result['usps_zip5'] = address_result['Zip5']
        clean_result['usps_zip_all'] = clean_result['usps_zip5']
      if 'Zip4' in result_keys:
        clean_result['usps_zip4'] = address_result['Zip4']
        clean_result['usps_zip_all'] = clean_result['usps_zip_all'] + '-' + address_result['Zip4']

      self.write( clean_result )




# define end points
def make_app():
  urls = [ (r"/address/", AddressHandler) ]
  return Application(urls)

# Start server  
if __name__ == '__main__':

    usps_secret = os.environ['USPS_SECRET']
    app = make_app()
    app.listen(8080, address='0.0.0.0')
    IOLoop.instance().start()

